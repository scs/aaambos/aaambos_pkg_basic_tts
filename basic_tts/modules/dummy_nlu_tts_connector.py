from __future__ import annotations

import uuid
from typing import Type

from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgTopicSendingAttribute, MsgPayloadWrapperCallbackReceivingAttribute
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
from simple_flexdiam.modules.core.definitions import SpeechGenerationControlComFeatureOut, NluResultsComFeatureIn, \
    SpeechGenerationControlComFeatureIn, NluResultsComFeatureOut, NluResults, \
    SpeechGenerationControl, Controls, AsrResultState, SPEECH_GENERATION_CONTROL, NLU_RESULTS


class DummyNluTtsConnector(Module, ModuleInfo):
    config: DummyNluTtsConnectorConfig
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            SpeechGenerationControlComFeatureOut.name: (SpeechGenerationControlComFeatureOut, SimpleFeatureNecessity.Required),
            NluResultsComFeatureIn.name: (NluResultsComFeatureIn, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            SpeechGenerationControlComFeatureIn.name: (SpeechGenerationControlComFeatureIn, SimpleFeatureNecessity.Required),
            NluResultsComFeatureOut.name: (NluResultsComFeatureOut, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return DummyNluTtsConnectorConfig

    async def initialize(self):
        await self.com.register_callback_for_promise(self.prm[NLU_RESULTS], self.handle_nlu_results)

    async def handle_nlu_results(self, topic, nlu_results: NluResults):
        if nlu_results.state == AsrResultState.FINAL:
            self.log.info(f"Received {nlu_results.utterance}")
            await self.com.send(self.tpc[SPEECH_GENERATION_CONTROL], SpeechGenerationControl(control=Controls.START, id=uuid.uuid4(), text=nlu_results.utterance))

    async def step(self):
        pass

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class DummyNluTtsConnectorConfig(ModuleConfig):
    module_path: str = "basic_tts.modules.dummy_nlu_tts_connector"
    module_info: Type[ModuleInfo] = DummyNluTtsConnector
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0


def provide_module():
    return DummyNluTtsConnector
