from __future__ import annotations

from typing import Type

from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
from aaambos.std.communications.attributes import MsgPayloadWrapperCallbackReceivingAttribute, MsgTopicSendingAttribute

import pyttsx3

from simple_flexdiam.modules.core.definitions import SpeechGenerationStatusComFeatureIn, \
    SpeechGenerationControlComFeatureOut, SpeechGenerationControlComFeatureIn, \
    SpeechGenerationControl, Controls, Status, \
    SpeechGenerationStatus, SpeechGenerationStatusComFeatureOut, SPEECH_GENERATION_STATUS, SPEECH_GENERATION_CONTROL


class BasicTts(Module, ModuleInfo):
    config: BasicTtsConfig
    engine = None
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            SpeechGenerationControlComFeatureIn.name: (SpeechGenerationControlComFeatureIn, SimpleFeatureNecessity.Required),
            SpeechGenerationStatusComFeatureOut.name: (SpeechGenerationStatusComFeatureOut, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            SpeechGenerationControlComFeatureOut.name: (SpeechGenerationControlComFeatureOut, SimpleFeatureNecessity.Required),
            SpeechGenerationStatusComFeatureIn.name: (SpeechGenerationStatusComFeatureIn, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return BasicTtsConfig

    async def initialize(self):
        self.engine = pyttsx3.init()

        # language  : en_US, de_DE, ...
        # gender    : VoiceGenderFemale, VoiceGenderMale
        def change_voice(engine, language):
            l_strs = []
            for voice in engine.getProperty('voices'):
                for l in voice.languages:
                    l_str = l if isinstance(l, str) else l.decode('utf-8')
                    if l_str.startswith("\x05"):
                        l_str = l_str[1:]
                    if l_str.startswith(language):
                        engine.setProperty('voice', voice.id)
                        return True
                    l_strs.append(l_str)
            engine.setProperty('rate', self.config.rate)
            print(language, l_strs)
            raise RuntimeError(f"Language '{language}' not found: available languages: {', '.join(l_strs)}")

        change_voice(self.engine, self.config.language)
        await self.com.register_callback_for_promise(self.prm[SPEECH_GENERATION_CONTROL], self.handle_tts_control)
        self.log.info(f"Init finished")

    async def step(self):
        pass

    async def handle_tts_control(self, topic, speech_control: SpeechGenerationControl):
        if speech_control.control == Controls.START:
            self.log.info(f"Verbalize {speech_control.control} {speech_control.text}")
            self.engine.say(speech_control.text)
            self.engine.runAndWait()
            await self.com.send(self.tpc[SPEECH_GENERATION_STATUS], SpeechGenerationStatus(id=speech_control.id, status=Status.finished))

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        self.engine.stop()
        return exit_code


@define(kw_only=True)
class BasicTtsConfig(ModuleConfig):
    module_path: str = "basic_tts.modules.basic_tts"
    module_info: Type[ModuleInfo] = BasicTts
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    language: str = "de"
    rate: int = 145
    """words per minute"""


def provide_module():
    return BasicTts
