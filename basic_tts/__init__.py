"""

This is the documentation of the aaambos package Basic Tts.
It contains aambos modules, owm run and/or arch configs,

# About the package
A TTS based on `pyttsx3` ([Github](https://github.com/nateshmbhat/pyttsx3)).


# Background / Literature

# Usage / Examples

## Installation

On Ubuntu install the packages required for **pyttsx3**.
```bash
sudo apt install espeak ffmpeg libespeak1
```

Running it on MacOS it just worked to install `py3-tts` via `pip install py3-tts`. On Windows you may be enough to do the steps below:

Install it via pip directly, if you don't want to change stuff in the module:
```bash
conda activate aaambos
pip install basic_tts @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_basic_tts@main
```
If you want to change stuff, clone it and install it via `pip install -e .`
Everytime you do `pip install -e .` it would unlink local installed (/cloned) aaambos and simple flexdiam installations and install the version from gitlab. You can prevent it by comment in the 2 requirements in the setup.py file.

## Running

The relevant new `arch_config` part (the last two are optional/the defaults):
```yaml
modules:
  basic_tts:
    module_info: !name:basic_tts.modules.basic_tts.BasicTts
    language: de
    rate: 145
```


# Citation


"""
